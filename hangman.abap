REPORT ZZ_08_HANGMAN NO STANDARD PAGE HEADING.

DATA: WORDS(20) OCCURS 10000 WITH HEADER LINE,
      SURRENDER(20),
      LINE(80), GUESSES OCCURS 10 WITH HEADER LINE,
                CORRECT_GUESSES OCCURS 10 WITH HEADER LINE,
                MESG(60),
      LEN TYPE I,
      NUM_WORDS TYPE I,
      NUM_TRYS TYPE I,
      NUM_FAILS TYPE I,
      NUM_BADS TYPE I,
      NUM_GOODS TYPE I,
      NUM_CHANGED TYPE STRING.
DATA  LETTER.

DATA: RUNT TYPE I, SEC TYPE I.

DATA: BEGIN OF HANGMAN OCCURS 10,
        X(2),
        Y(2),
        L,
      END OF HANGMAN.

INITIALIZATION.
  APPEND '7506|' TO HANGMAN.
  APPEND '75070' TO HANGMAN.
  APPEND '7508|' TO HANGMAN.
  APPEND '7408/' TO HANGMAN.
  APPEND '7409/' TO HANGMAN.
  APPEND '7609\' TO HANGMAN.
  APPEND '7608\' TO HANGMAN.
  MOVE 'test' TO LINE.  APPEND LINE TO WORDS.
  MOVE 'foo' TO LINE.  APPEND LINE TO WORDS.
  MOVE 'hallo' TO LINE.  APPEND LINE TO WORDS.
  MOVE 'abend' TO LINE.  APPEND LINE TO WORDS.
  MOVE 'aquarium' TO LINE.  APPEND LINE TO WORDS.
  MOVE 'antilope' TO LINE.  APPEND LINE TO WORDS.
  MOVE 'bildschirm' TO LINE.  APPEND LINE TO WORDS.
  MOVE 'bahnhof' TO LINE.  APPEND LINE TO WORDS.
  MOVE 'besserwisser' TO LINE.  APPEND LINE TO WORDS.
  MOVE 'chlor' TO LINE.  APPEND LINE TO WORDS.
  MOVE 'chemie' TO LINE.  APPEND LINE TO WORDS.
  MOVE 'dienstreise' TO LINE.  APPEND LINE TO WORDS.
  MOVE 'dach' TO LINE.  APPEND LINE TO WORDS.
  MOVE 'drehmoment' TO LINE.  APPEND LINE TO WORDS.
  MOVE 'eule' TO LINE.  APPEND LINE TO WORDS.
  MOVE 'essensmarke' TO LINE.  APPEND LINE TO WORDS.
  MOVE 'einschreiben' TO LINE.  APPEND LINE TO WORDS.
  MOVE 'futter' TO LINE.  APPEND LINE TO WORDS.
  MOVE 'fackeln' TO LINE.  APPEND LINE TO WORDS.
  MOVE 'fledermaus' TO LINE.  APPEND LINE TO WORDS.
  MOVE 'gras' TO LINE.  APPEND LINE TO WORDS.
  MOVE 'grundriss' TO LINE.  APPEND LINE TO WORDS.
  MOVE 'geizhals' TO LINE.  APPEND LINE TO WORDS.
  MOVE 'holzkohle' TO LINE.  APPEND LINE TO WORDS.
  MOVE 'haken' TO LINE.  APPEND LINE TO WORDS.
  MOVE 'informatiker' TO LINE.  APPEND LINE TO WORDS.
  MOVE 'imker' TO LINE.  APPEND LINE TO WORDS.
  MOVE 'indien' TO LINE.  APPEND LINE TO WORDS.
  MOVE 'jagdgewehr' TO LINE.  APPEND LINE TO WORDS.
  MOVE 'kasse' TO LINE.  APPEND LINE TO WORDS.
  MOVE 'langlauf' TO LINE.  APPEND LINE TO WORDS.
  MOVE 'linux' TO LINE.  APPEND LINE TO WORDS.
  MOVE 'luft' TO LINE.  APPEND LINE TO WORDS.
  MOVE 'milch' TO LINE.  APPEND LINE TO WORDS.
  MOVE 'mitternacht' TO LINE.  APPEND LINE TO WORDS.
  MOVE 'maschine' TO LINE.  APPEND LINE TO WORDS.
  MOVE 'nest' TO LINE.  APPEND LINE TO WORDS.
  MOVE 'nummernschild' TO LINE.  APPEND LINE TO WORDS.
  MOVE 'nutzer' TO LINE.  APPEND LINE TO WORDS.
  MOVE 'orchester' TO LINE.  APPEND LINE TO WORDS.
  MOVE 'personalmangel' TO LINE.  APPEND LINE TO WORDS.
  MOVE 'qualle' TO LINE.  APPEND LINE TO WORDS.
  MOVE 'regenbogen' TO LINE.  APPEND LINE TO WORDS.
  MOVE 'richter' TO LINE.  APPEND LINE TO WORDS.
  MOVE 'raub' TO LINE.  APPEND LINE TO WORDS.
  MOVE 'schlange' TO LINE.  APPEND LINE TO WORDS.
  MOVE 'sonne' TO LINE.  APPEND LINE TO WORDS.
  MOVE 'struktur' TO LINE.  APPEND LINE TO WORDS.
  MOVE 'tempel' TO LINE.  APPEND LINE TO WORDS.
  MOVE 'tauchschein' TO LINE.  APPEND LINE TO WORDS.
  MOVE 'umlage' TO LINE.  APPEND LINE TO WORDS.
  MOVE 'verkehrsschild' TO LINE.  APPEND LINE TO WORDS.
  MOVE 'xylophon' TO LINE.  APPEND LINE TO WORDS.
  MOVE 'yoga' TO LINE.  APPEND LINE TO WORDS.
  MOVE 'zahnarzt' TO LINE.  APPEND LINE TO WORDS.
  MOVE 'ziel' TO LINE.  APPEND LINE TO WORDS.


  DESCRIBE TABLE WORDS LINES NUM_WORDS.

  PERFORM PRINT_OUT.

  DATA: ABCDE LIKE SY-ABCDE.
*---------------------------------------------------------------------*
*       FORM PRINT_OUT                                                *
*---------------------------------------------------------------------*
FORM PRINT_OUT.
  DATA: WORD_MASK LIKE WORDS, INDEX TYPE I.
  ABCDE = SY-ABCDE.
  SY-LSIND = 0.
  WRITE:/32 'HANGMAN'.
  WRITE:/.
  WRITE:/.
  ULINE: AT /14(14), AT 29(13), AT 43(13).
  WRITE:/13'' NO-GAP.
  FORMAT COLOR COL_TOTAL.
  WRITE:'|', 'Neues Wort' HOTSPOT,
     '|' NO-GAP, SPACE NO-GAP COLOR COL_BACKGROUND,
          '|', 'Aufdecken' HOTSPOT,
     '|' NO-GAP, SPACE NO-GAP COLOR COL_BACKGROUND,
          '|','Verlassen' HOTSPOT,'|'.
  FORMAT RESET.
  ULINE: AT /14(14), AT 29(13), AT 43(13).
  WRITE:/.

  IF NOT WORDS IS INITIAL.
*   print the word mask
    LEN = STRLEN( WORDS ).
    DO.
      INDEX =  SY-INDEX - 1.
      LETTER = WORDS+INDEX(1).
      SEARCH  CORRECT_GUESSES FOR LETTER.
      IF SY-SUBRC = 0.
        WORD_MASK+INDEX = LETTER.
      ELSE.
        WORD_MASK+INDEX = '_'.
      ENDIF.
      IF LEN = SY-INDEX .
        EXIT.
      ENDIF.
    ENDDO.

*   found the word
    IF WORD_MASK = WORDS.
      ADD 1 TO NUM_GOODS.
      WRITE:/10 'Erraten! Das Wort war:',WORDS,
            /10 'Sie haben',(2)NUM_TRYS,'Versuche benötigt.'.
      
      PERFORM DRAW_HANGMAN.
      
      CLEAR: NUM_TRYS, NUM_FAILS, WORDS.

      NUM_CHANGED = 'NUM_GOODS'.
      PERFORM PRINT_MSG.
    ELSE.
      WRITE:/10  'Erraten Sie folgendes Wort:' .
      DO LEN TIMES.
        INDEX = SY-INDEX - 1.
        LETTER = WORD_MASK+INDEX.
        WRITE: LETTER.
      ENDDO.
      WRITE:/10 'Sie haben maximal 6 Fehlversuche.'.
      WRITE: /.

*     print guesses
      WRITE: /10  'Bisher geraten:'.
      SORT GUESSES.

      LOOP
       AT GUESSES.
        WRITE:  GUESSES CENTERED.
      ENDLOOP.

      WRITE: /.

*     check max failures
      IF NUM_FAILS < 7.
*     print keyboard
        ULINE AT /8(55).
        DO 26 TIMES.
          LETTER = ABCDE.
          IF SY-INDEX = 14 OR SY-INDEX = 1. WRITE:/8  SPACE. ENDIF.
          WRITE: (3)LETTER CENTERED HOTSPOT COLOR COL_KEY .
          SHIFT ABCDE.
        ENDDO.
         WRITE:/.
        WRITE:/8 MESG.
      ELSE.
        WRITE:/10 'Das richtige Wort lautete:',WORDS.
        ADD 1 TO NUM_BADS.
        CLEAR WORDS.

        NUM_CHANGED = 'NUM_BADS'.
        PERFORM PRINT_MSG.
      ENDIF.
      PERFORM DRAW_HANGMAN.
    ENDIF.
  ELSEIF NOT SURRENDER IS INITIAL.
    WRITE:/10 'Das richtige Wort lautete:',SURRENDER.
    CLEAR SURRENDER.
  ENDIF.
ENDFORM.
*---------------------------------------------------------------------*
*       FORM PRINT_MSG                                                *
*---------------------------------------------------------------------*
FORM PRINT_MSG.
  DATA: MESG2 TYPE string,
        TMP_NUM_GOODS TYPE N,
        TMP_NUM_BADS TYPE N.
  TMP_NUM_GOODS = NUM_GOODS.
  TMP_NUM_BADS = NUM_BADS.
  SHIFT TMP_NUM_GOODS LEFT DELETING LEADING '0'.
  SHIFT TMP_NUM_BADS LEFT DELETING LEADING '0'.

  SKIP TO LINE 19.

  IF NUM_CHANGED = 'NUM_GOODS'.
    IF NUM_GOODS = 1.
      CONCATENATE 'Glückwunsch! Sie haben' 'Ihr erstes Wort erraten!' INTO MESG2 SEPARATED BY SPACE.
      ULINE AT /8(51).
      WRITE:/8 '|', MESG2, '|'.
      ULINE AT /8(51).
    ELSEIF NUM_GOODS > 1.
      CONCATENATE 'Sie haben' TMP_NUM_GOODS 'Wörter erraten!' INTO MESG2 SEPARATED BY SPACE.
      ULINE AT /8(31).
      WRITE:/8 '|', MESG2, '|'.
      ULINE AT /8(31).
    ENDIF.
  ELSE.
    IF NUM_BADS = 1.
      CONCATENATE 'Schade!' 'Sie haben das Wort nicht erraten!' INTO MESG2 SEPARATED BY SPACE.
      ULINE AT /8(45).
      WRITE:/8 '|', MESG2, '|'.
      ULINE AT /8(45).
    ELSE.
      CONCATENATE 'Mittlerweile haben Sie' TMP_NUM_BADS 'Wörter nicht erraten!' INTO MESG2 SEPARATED BY SPACE.
      ULINE AT /8(50).
      WRITE:/8 '|', MESG2, '|'.
      ULINE AT /8(50).
    ENDIF.
  ENDIF.

  CLEAR MESG2.
  WRITE:/ MESG2.
ENDFORM.
*---------------------------------------------------------------------*
*       FORM RANDOM                                                   *
*---------------------------------------------------------------------*
FORM RANDOM.
  GET RUN TIME FIELD RUNT.
  GET TIME.
  SEC = SY-UZEIT MOD 60.
  RUNT = RUNT MOD 100000.
  MULTIPLY RUNT BY SEC.
  GET TIME.
  RUNT = RUNT MOD 100000.
  ADD SEC TO RUNT .
ENDFORM.

AT LINE-SELECTION.
  DATA: FIELD(30),VALUE(50).
  GET CURSOR FIELD FIELD VALUE VALUE.
  IF VALUE = 'Neues Wort'.
    PERFORM NEW_WORD.
  ELSEIF VALUE = 'Aufdecken'.
    PERFORM GIVE_UP.
  ELSEIF VALUE = 'Verlassen'.
    LEAVE PROGRAM.
  ELSEIF FIELD = 'LETTER' AND NOT VALUE IS INITIAL.
    PERFORM TRY_LETTER USING VALUE+1(1).
  ENDIF.

*&---------------------------------------------------------------------*
*&      Form  NEW_WORD
*&---------------------------------------------------------------------*
FORM NEW_WORD.
  CLEAR: MESG, WORDS.
  IF NOT WORDS IS INITIAL.
    ADD 1 TO NUM_BADS.
  ENDIF.
  DO.
    PERFORM RANDOM.
    RUNT = RUNT MOD NUM_WORDS.
    ADD 1 TO RUNT.
    READ TABLE WORDS INDEX RUNT.
    IF NOT WORDS(1) CO SY-ABCDE AND WORDS NA '0123456789'.
      EXIT.
    ENDIF.
  ENDDO.
  TRANSLATE WORDS TO UPPER CASE.
  CLEAR: NUM_TRYS, NUM_FAILS.
  REFRESH: GUESSES, CORRECT_GUESSES.
  PERFORM PRINT_OUT.
ENDFORM.                               " NEW_WORD

*&---------------------------------------------------------------------*
*&      Form  TRY_LETTER
*----------------------------------------------------------------------*
FORM TRY_LETTER USING   P_VALUE TYPE C.
  SY-LSIND = 0.
  SEARCH GUESSES FOR P_VALUE.
  IF SY-SUBRC = 0.
    CONCATENATE 'Sie haben es mit dem Buchstaben' P_VALUE 'bereits versucht.'
                                           INTO MESG SEPARATED BY SPACE.
  ELSE.
    APPEND P_VALUE TO GUESSES .
    IF WORDS CS P_VALUE.
      CONCATENATE 'Sie haben den Buchstaben' P_VALUE 'erraten!'
                                           INTO MESG SEPARATED BY SPACE.
      APPEND P_VALUE TO CORRECT_GUESSES .
    ELSE.
      CONCATENATE 'Der Buchstabe' P_VALUE 'ist nicht im Wort enthalten.'
                                          INTO MESG SEPARATED BY SPACE.
      ADD 1 TO NUM_FAILS.
    ENDIF.
    ADD 1 TO NUM_TRYS.
  ENDIF.
  PERFORM PRINT_OUT.
ENDFORM.                               " TRY_LETTER


AT PF8.
  PERFORM NEW_WORD.
*&---------------------------------------------------------------------*
*&      Form  GIVE_UP
*&---------------------------------------------------------------------*
FORM GIVE_UP.
  ADD 1 TO NUM_BADS.
  SURRENDER = WORDS.
  CLEAR WORDS.
  PERFORM PRINT_OUT.
ENDFORM.                               " GIVE_UP

*---------------------------------------------------------------------*
*       FORM DRAW_HANGMAN                                                 *
*---------------------------------------------------------------------*
FORM DRAW_HANGMAN.
*   |
*   0
*  /|\
*  / \
*
  BACK.
  SKIP 4.
  WRITE: /70 '  -------'.
  WRITE: /70 '  |'.
  WRITE: /70 '  |'.
  WRITE: /70 '  |'.
  WRITE: /70 '  |'.
  WRITE: /70 '  |'.
  WRITE: /70 '---------'.

  LOOP
   AT HANGMAN TO NUM_FAILS.
    BACK.
    SKIP TO LINE HANGMAN-Y.
    WRITE: AT HANGMAN-X HANGMAN-L.
  ENDLOOP.
  BACK.
ENDFORM.