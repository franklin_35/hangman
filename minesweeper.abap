*&---------------------------------------------------------------------*
*& Report  ZZ_TESTMIN
*&
*&---------------------------------------------------------------------*
*&
*&
*&---------------------------------------------------------------------*

REPORT  ZZ_TESTMIN.

INCLUDE <icon>.

CONSTANTS:
  head_line Type i Value 4,"         Überschrift Zeile 1
  head_line2 Type i Value 5,"         Überschrift Zeile 2
  head_line3 Type i Value 6,"         Überschrift Zeile 3
    line_ini TYPE i VALUE 13,"   Erste Zeile - Beginns der Schrift
  pos_ini TYPE i VALUE 43,"         Position - Beginn der Tabelle
  lines TYPE i VALUE 10,"                    Anzahl Zeilen der Tabelle
  columns TYPE i VALUE 10,"              Anzahl Spalten der Tabelle
  bomb(3) VALUE ' # ',"                           Symbol der Bombe
  detect(3) VALUE ' B ',"                   Symbol des Bombendetektors
  line_msg2 TYPE i VALUE 8,"               Zeile der Verbleibenden Bomben-Nachricht
  line_msg TYPE i VALUE 9,"                Zeile der Nachricht
  line_button TYPE i VALUE 11,"             Zeile des Buttons
  num_bomb(2) TYPE n VALUE 3," Anzahl der Bomben
  max_mist(2) TYPE n VALUE 2," Anzahl moeglicher Fehler
  button_on(17)  VALUE '<Feld aufdecken>',
  button_off(17) VALUE '<Bombe markieren>',
  icon_on  LIKE icons-l2 VALUE '@0V@',
  icon_off LIKE icons-l2 VALUE '@7W@',
  icon_err Like icont-id VALUE '@3C@'.

DATA:
  head(300),"         Überschrift Zeile 1
  head2(300),"         Überschrift Zeile 2
  head3(300),"         Überschrift Zeile 3
  msg_1(100),"                                                   Nachricht
  msg_2(45),"                                 Verbleibende Bomben-Anzeige
  game_over,"                                  Flag für das Ende eines Spiels
  str_tmp(255),"                                Temporäre String-Variable
  button(17),"                                          Text des Buttons
  icon_button LIKE icont-id,"      Dem Symbol wird mit einem Button verknüpft
  radar_counter TYPE i,"          Zähler für angrenzende "gefährliche" Zellen
  error_counter TYPE i,"                             Fehlerzähler
  detect_counter TYPE i,"      Zähler für als Bomben markierte Zellen
  general_counter TYPE i,"                    detect_counter + error_counter
  cell_name LIKE dd03d-fieldname,"       Zellenname
  raw      TYPE i,"                   Zeile der Zelle
  column   TYPE i,"                Spalte der Zelle
  raw_char2(2),
  column_char2(2).

DATA:
  BEGIN OF itab_let,
    1(3), 2(3), 3(3), 4(3), 5(3), 6(3), 7(3), 8(3), 9(3), 10(3),
  END OF itab_let,

* Struktur aller Zellen der Tabelle
  BEGIN OF itab,
    1 LIKE itab_let, 2 LIKE itab_let, 3 LIKE itab_let,
    4 LIKE itab_let, 5 LIKE itab_let, 6 LIKE itab_let,
    7 LIKE itab_let, 8 LIKE itab_let, 9 LIKE itab_let,
    10 LIKE itab_let,
  END OF itab,

* Struktur für ausgewählte Bomben

  bis_itab LIKE itab,

*Struktur für alle initial berechneten Bomben (Anfangsstruktur)

  bomb_itab LIKE itab.
FIELD-SYMBOLS: <fs1>, <fs2>, <fs3>, <fs4>.

************************************************************************
*                             INITIALIZATION                        *
************************************************************************

INITIALIZATION.

************************************************************************
*                          START-OF-SELECTION                       *
************************************************************************

START-OF-SELECTION.
  PERFORM display_table.
  PERFORM assign_bombs.

************************************************************************
*                         AT LINE-OF-SELECTION                      *
************************************************************************

AT LINE-SELECTION.
  GET CURSOR FIELD cell_name.

*----------------------------------------------------------------------*
*                      Ändern des Textes auf dem Button                *
*----------------------------------------------------------------------*
* Nur, wenn das Spiel noch nicht aus ist. Wenn ein Spiel beendet ist, bleibt der Button auf ON.

  IF cell_name = 'BUTTON' AND game_over IS INITIAL.
    PERFORM change_button.
  ENDIF.

*----------------------------------------------------------------------*
*    Übernimm den Wert der Selektierten Zelle in alle weiteren Tabellen*
*----------------------------------------------------------------------*

  CHECK cell_name CS 'ITAB-'.
  ASSIGN (cell_name) TO <fs1>." ------------------- Aktuelle Tabelle
  CONCATENATE 'BIS_' cell_name INTO str_tmp.
  ASSIGN (str_tmp) TO <fs2>." --------------------- Tabelle der markierten Bomben
  CONCATENATE 'BOMB_' cell_name INTO str_tmp.
  ASSIGN (str_tmp) TO <fs3>." --------------- Tabelle aller Bomben
  SUBTRACT 1 FROM sy-lsind.

*&====================================================================&*
*&                           Bombendetektor                          &*
*&             MIT 'D' die ausgewählte Zelle markierten              &*
*&====================================================================&*

  IF button = button_off.
    DATA:
          counter_char(3),
          remaining_marks TYPE i.

*   Überprüfen, dass es keine Bombe ist, die nicht markiert und oder ermittelt wurde.

    CHECK <fs1> NE bomb AND <fs1> NE detect.
    MODIFY LINE sy-lilli FIELD VALUE  <fs1> FROM detect
                         FIELD FORMAT <fs1> COLOR col_total.
    <fs1> = detect.
    ADD 1 TO detect_counter.
          general_counter = error_counter + detect_counter.

    remaining_marks = num_bomb - detect_counter.

    MOVE remaining_marks TO counter_char.
    CONCATENATE 'Verbleibende Markierungen: ' counter_char INTO msg_2.
    CLEAR sy-lisel.
    MODIFY LINE line_msg2 FIELD VALUE msg_2.

    IF general_counter = num_bomb.
      PERFORM check_success.
    ENDIF.
    EXIT.
  ENDIF.
*&====================================================================&*
*&  Zeige ob in der Zelle eine Bombe oder eine sie umgebende Zelle ist&*
*&                                                                    &*
*&====================================================================&*

  IF <fs1> = detect. SUBTRACT 1 FROM detect_counter. ENDIF.
*Wenn das Spiel zu Ende ist, dürfen keine Bomben mehr markiert werden
* Wenn die Zelle eine Bombe ist, markiere sie mit   ' B '

IF game_over <> 'X'.
  IF <fs3> = bomb.
    PERFORM x_bomb.

* Wenn die Zelle keine Bombe ist, zeige angrezende Zellen an

  ELSE.
    PERFORM obtain_coordinates.
    PERFORM obtain_limits.
  ENDIF.
  SET CURSOR 0 0.
ENDIF.

*&---------------------------------------------------------------------*
*&      Form  DISPLAY_TABLE
*&---------------------------------------------------------------------*

FORM display_table.
  DATA: cablet(255), longitud TYPE i, numraw(2), n TYPE i, remaining_marks TYPE i, counter_char TYPE c.
  SKIP TO LINE line_ini.
  cablet = '  | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10|'.
  longitud = strlen( cablet ).
  WRITE AT pos_ini cablet.
  ULINE AT /pos_ini(longitud).
  NEW-LINE.

  DO lines TIMES." -------------------------------------------  lines
    MOVE sy-index TO numraw.
    ASSIGN COMPONENT sy-index OF STRUCTURE itab TO <fs1>.
    POSITION pos_ini.
    WRITE: numraw NO-GAP RIGHT-JUSTIFIED.
    WRITE: sy-vline NO-GAP.

    DO columns TIMES." -----------------------------------   columns
      ASSIGN COMPONENT sy-index OF STRUCTURE <fs1> TO <fs2>.
      WRITE: <fs2> HOTSPOT NO-GAP,
             sy-vline NO-GAP.
    ENDDO.
    ULINE AT /pos_ini(longitud). NEW-LINE.
  ENDDO.

* ÜBERSCHRIFT IN ASCII-CODE

SKIP TO LINE head_line." ---------------------------------  Überschrift Zeile 1
  head = '  \  |_ _|  \ | __|  __|\ \      / __| __| _ \ __| _ \'.
  WRITE AT: pos_ini head INTENSIFIED OFF,
            sy-linsz space.

  SKIP TO LINE head_line2." ---------------------------------  Überschrift Zeile 2
  head2 = ' |\/ |  |  .  | _| \__ \ \ \ \  /  _|  _|  __/ _|    /'.
  WRITE AT: pos_ini head2 INTENSIFIED OFF,
            sy-linsz space.

  SKIP TO LINE head_line3." ---------------------------------  Überschrift Zeile 3
  head3 = '_|  _|___|_|\_|___|____/  \_/\_/  ___|___|_|  ___|_|_\'.
  WRITE AT: pos_ini head3 INTENSIFIED OFF,
            sy-linsz space.


  SKIP TO LINE line_msg2." ---------------------------------  Nachricht
  remaining_marks = num_bomb.
  MOVE remaining_marks to counter_char.
  CONCATENATE `Verbleibende Markierungen: ` counter_char INTO msg_2.
  WRITE AT: pos_ini msg_2 INTENSIFIED OFF,
            sy-linsz space.

  SKIP TO LINE line_msg." ---------------------------------  Nachricht
  msg_1 = 'Getroffene Bomben: 0  '.
  WRITE AT: pos_ini msg_1 INTENSIFIED OFF,
            sy-linsz space.
  SKIP 1.
  icon_button = icon_on." ----------------------------- Icon des Buttons
  WRITE AT pos_ini icon_button AS ICON.
  button = button_on." --------------------------------- Text des Buttons
  CLEAR n.
  n = pos_ini + 4.
  WRITE AT n button HOTSPOT COLOR COL_POSITIVE.
ENDFORM.                    " DISPLAY_TABLE

*&---------------------------------------------------------------------*
*&      Form  assign_bombS
*&---------------------------------------------------------------------*
* F+lle die Tabelle BOMB_ITAB mit der gewählten Anzahl an Bomben
*&---------------------------------------------------------------------*

FORM assign_bombs.
  DATA: char2(2) TYPE c, fieldname(255), bomb_counter TYPE i.

  PERFORM ini_spread.
  WHILE bomb_counter LT num_bomb.

*------------------------------------------------------------------ raw

    CALL FUNCTION 'QF05_RANDOM_INTEGER'
         EXPORTING
              ran_int_max = 10
              ran_int_min = 1
         IMPORTING
              ran_int     = raw
         EXCEPTIONS
              OTHERS      = 2.
    MOVE raw TO char2.
    CONCATENATE 'BOMB_ITAB-' char2 INTO fieldname.

*--------------------------------------------------------------- Column

    CALL FUNCTION 'QF05_RANDOM_INTEGER'
         EXPORTING
              ran_int_max = 10
              ran_int_min = 1
         IMPORTING
              ran_int     = column
         EXCEPTIONS
              OTHERS      = 1.
    MOVE column TO char2.
    CONCATENATE fieldname '-' char2 INTO fieldname.

    ASSIGN (fieldname) TO <fs1>.
    IF <fs1> IS INITIAL.
      <fs1> = bomb.
      ADD 1 TO bomb_counter.
    ENDIF.
  ENDWHILE.
ENDFORM.                    " assign_bombS

*&---------------------------------------------------------------------*
*&      Form  INI_spread
*&---------------------------------------------------------------------*
* Streuung der Bomben soll zufällig sein:
*----------------------------------------------------------------------*

FORM ini_spread.
  DO 5 TIMES.
      CALL FUNCTION 'QF05_RANDOM_INTEGER'
      EXPORTING
        RAN_INT_MAX         = 10
        RAN_INT_MIN         = 1
      IMPORTING
        RAN_INT             = raw
      EXCEPTIONS
        OTHERS              = 2.
  ENDDO.
ENDFORM.                    " INI_spread

*&---------------------------------------------------------------------*
*&      Form  OBTAIN_coordinates
*&---------------------------------------------------------------------*

FORM obtain_coordinates.

  DATA  string_components LIKE dd03d-fieldname OCCURS 0.
  SPLIT cell_name AT '-' INTO TABLE string_components.
  READ TABLE string_components INDEX 2 INTO raw.
  READ TABLE string_components INDEX 3 INTO column.
ENDFORM.                    " OBTAIN_coordinates

*&---------------------------------------------------------------------*
*&      Form  OBTAIN_LIMITS
*&---------------------------------------------------------------------*
* So findet man heraus, ob die Zelle an eine markierte Bombe angrenzt:
*----------------------------------------------------------------------*

FORM obtain_limits.

  CLEAR radar_counter.

* Zelle N

  raw_char2    = raw - 1.
  column_char2 = column.
  PERFORM radar.

* Zelle NW

  raw_char2 = raw - 1.
  column_char2 = column - 1.
  PERFORM radar.

* Zelle NE

  raw_char2 = raw - 1.
  column_char2 = column + 1.
  PERFORM radar.

* Zelle S

  raw_char2 = raw + 1.
  column_char2 = column.
  PERFORM radar.

* Zelle SW

  raw_char2 = raw + 1.
  column_char2 = column - 1.
  PERFORM radar.

* Zelle SE

  raw_char2 = raw + 1.
  column_char2 = column + 1.
  PERFORM radar.

* Zelle E


  raw_char2 = raw.
  column_char2 = column - 1.
  PERFORM radar.

* Zelle W

  raw_char2 = raw.
  column_char2 = column + 1.
  PERFORM radar.
  <fs1> = radar_counter.
  MODIFY LINE sy-lilli FIELD VALUE <fs1>  FROM radar_counter
                      FIELD FORMAT <fs1> COLOR COL_POSITIVE.
ENDFORM.                    " OBTAIN_LIMITS

*&---------------------------------------------------------------------*
*&      Form  RADAR
*&---------------------------------------------------------------------*
* Hier findet man heraus, ob die ausgewählte Zelle eine Bombe ist:
*----------------------------------------------------------------------*

FORM radar.

  CHECK raw_char2 NE '0'  AND column_char2 NE '0'  AND
        raw_char2 NE '11' AND column_char2 NE '11'.
  CONCATENATE 'BOMB_ITAB-' raw_char2 '-' column_char2 INTO str_tmp.
  ASSIGN (str_tmp) TO <fs3>.
  IF <fs3> = bomb. ADD 1 TO radar_counter. ENDIF.
ENDFORM.                                                    " RADAR

*&---------------------------------------------------------------------*
*&      Form  X_bomb
*&---------------------------------------------------------------------*
* Markiere eine Bombe in der Tabelle und zeige die passende Nachricht an
*----------------------------------------------------------------------*

FORM x_bomb.
  DATA counter_char(3).


* Check, ob Bombe zuvor bereits ausgewählt wurde

  CHECK <fs1> NE bomb.

* Bombe in Tabelle markieren

  <fs1> = bomb.
  MODIFY LINE sy-lilli FIELD VALUE <fs1>  FROM <fs1>
                       FIELD FORMAT <fs1> COLOR col_negative.

* Hier geht es nur weiter, wenn das Spiel nicht vorbei ist

  CHECK game_over IS INITIAL.
  ADD 1 TO error_counter.
  MOVE error_counter TO counter_char.
  CONCATENATE 'Getroffene Bomben: ' counter_char INTO msg_1.
  CLEAR sy-lisel.
  MODIFY LINE line_msg FIELD VALUE msg_1.



  IF error_counter = max_mist.
    msg_1 =  'Leider haben Sie zu viele Bomben erwischt!'.
    MODIFY LINE line_msg FIELD VALUE msg_1
                         FIELD FORMAT msg_1 COLOR col_negative
                         INVERSE.
    game_over = 'X'.
  ENDIF.

  general_counter = error_counter + detect_counter.
  IF general_counter = num_bomb.
    PERFORM check_success.
  ENDIF.
ENDFORM.                    " X_bomb

*&---------------------------------------------------------------------*
*&      Form  CHANGE_button
*&---------------------------------------------------------------------*
* Ändere den Namen des Buttons von button_ON in button_OFF und umgekehrt:
*----------------------------------------------------------------------*

FORM change_button.

  DATA: button_tmp LIKE button, color_tmp TYPE i, new_icon LIKE
icons-l2.

  CASE button.
    WHEN button_on.
      button = button_off. new_icon = icon_off. color_tmp = 3.
    WHEN button_off.
      button = button_on. new_icon = icon_on. color_tmp = 5.
  ENDCASE.

  WRITE button TO button_tmp.
  icon_prepare_for_modify new_icon.

  MODIFY LINE line_button FIELD VALUE button FROM button_tmp
                                     icon_button FROM new_icon
                         FIELD FORMAT button COLOR = color_tmp.

  SET CURSOR 1 1.
ENDFORM.                    " CHANGE_button

*&---------------------------------------------------------------------*
*&      Form  CHECK_SUCCESS
*&---------------------------------------------------------------------*
* Das Spiel ist beendet. Prüfe, ob der Spielger gewonnen hat
*----------------------------------------------------------------------*

FORM check_success.

  DATA counter_remain(3).

  DATA: n TYPE i, m TYPE i, raw_tmp TYPE i, column_tmp TYPE i,
        raw_tmp_float TYPE f, raw_char3(3), column_char3(3),
        new_icon LIKE icons-l2.
  FIELD-SYMBOLS: <fs5>, <fs6>.
  n = lines * columns.

* Para mostrar un NUEVO listado con las detecciones incorrectas.

  ADD 1 TO sy-lsind.



* ---------------------------------------------------------------------*
* raw_TMP y COLUMN_TMP guardan las coordinates de la Zelle a partir de
* su posicion absoluta (SY-INDEX)
* Por ejemplo, el campo 19 esta en la segunda raw, novena column; para
* un total de 10 lines y 10 columns
*----------------------------------------------------------------------*

  DO n TIMES.
    raw_tmp_float = sy-index / columns.
    raw_tmp = ceil( raw_tmp_float ).
    column_tmp = sy-index - ( columns * raw_tmp ) + columns.
    MOVE: raw_tmp TO raw_char3, column_tmp TO column_char3.
    CONCATENATE 'ITAB-' raw_char3 '-' column_char3
    INTO str_tmp.
    CONDENSE str_tmp NO-GAPS.
    ASSIGN (str_tmp) TO <fs6>.

*   Hier wird festgestellt, ob eine mit 'D' markierte Zelle auach wirklich eine Bombe ist

    IF <fs6> = detect.
      CONCATENATE 'BOMB_ITAB-' raw_char3 '-' column_char3
      INTO str_tmp.
      CONDENSE str_tmp NO-GAPS.
      ASSIGN (str_tmp) TO <fs5>.
      IF <fs5> NE bomb.

*       Nachricht mit Liste falsch markierter Zellen

        CONCATENATE 'Die Zelle ' raw_char3 '-' column_char3
        ' enthält keine Bombe' INTO str_tmp.
        WRITE str_tmp.
        msg_1 = 'Sie haben verloren! Versuchen Sie es doch noch einmal!'.
        MODIFY LINE line_msg FIELD VALUE msg_1
                             FIELD FORMAT msg_1 COLOR col_negative
                                                INVERSE.
        game_over = 'X'." ------------------------------ Spiel vorbei
      ENDIF.
    ENDIF.
  ENDDO.

* Button auf ON schalten (es wird nicht mehr erlaubt weitere Bomben zu markieren,
* da nicht mehr zu erkennen ist, ob das Spiel gewonnen werden kann)

  new_icon = icon_on.
  icon_prepare_for_modify new_icon.
  button = button_on.
  CLEAR sy-lisel.
  MODIFY LINE line_button FIELD VALUE  icon_button FROM new_icon
                                      button
                         FIELD FORMAT button COLOR = 5.

* Nachricht über gewonnenes Spiel (nicht, wenn keine Bombe markiert wurde???)

  CHECK game_over IS INITIAL.
  msg_1 = 'Glückwunsch! Sie haben gewonnen!'.
  MODIFY LINE line_msg FIELD VALUE  msg_1
                       FIELD FORMAT msg_1 COLOR col_total INVERSE.
  game_over = 'X'.
ENDFORM.                    " CHECK_SUCCESS